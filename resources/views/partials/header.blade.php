<header class="banner">
  <nav class="navbar navbar-light navbar-expand-md bg-light justify-content-center">
    <div href="/" class="navbar-brand d-flex w-50 mr-auto"></div>
    <div class="navbar-collapse w-100">
        <div class="navbar-nav w-100 justify-content-center">
          <a class="navbar-brand" href="{{ home_url('/') }}">
            {{ get_bloginfo( 'name' ) }}
            @php
              $custom_logo_id = get_theme_mod( 'custom_logo' );
              $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            @endphp
            @if ( has_custom_logo() ) 
              <img src="{{esc_url($logo[0])}}" width="30" height="30" class="d-inline-block align-top" alt="{{get_bloginfo( 'name' )}}">
            @endif
          </a>
        </div>
        <div class="nav navbar-nav ml-auto w-100 justify-content-end">
          <?php get_search_form();?>
        </div>
    </div>
  </nav>

  <div class="container">
    <nav class="nav-primary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav justify-content-center']) !!}
      @endif
    </nav>
  </div>
</header>
